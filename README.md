Program to manage iptables filter and nat rules and change policies.

Requires python to be installed and must be run with sudo as effective user id.

To run the program simply run
```
python3 iptables_manager.py
```