import subprocess
import os
import re
from time import sleep

def stdout_erase_line(count = 1):
    CURSOR_UP_ONE = '\x1b[1A'
    ERASE_LINE = '\x1b[2K'

    for i in range(count):
        print(CURSOR_UP_ONE + ERASE_LINE + CURSOR_UP_ONE)

def print_section(header):
    print('\n')
    print(header)
    print('='*50)

def print_enum_list(_list):
    for nr, entry in enumerate(_list):
        if(entry == ''):
            print(str(nr+1)+'.', '<- Back')
        else:
            print(str(nr+1)+'.', entry)

def def_input(text, def_val):
    print('(Leave blank for {})'.format(def_val))
    inp = input(text)
    stdout_erase_line(2)
    if inp is '':
        inp = def_val
    return inp

def continue_prompt():
    print('\nPress enter to continue')
    input()

def nr_input(symbol = '\n->', min_bound = float('-inf'), max_bound = float('inf')):

    def __bad_input():
        print('Wrong input entered')
        sleep(0.5)
        stdout_erase_line()
        return None

    inp = input(symbol + ' ')
    stdout_erase_line()

    try:
        inp = int(inp)
    except:
        return __bad_input()

    if min_bound <= inp <= max_bound:
        return inp 
    else:
        return __bad_input()

def yn_prompt(symbol = '->', text = 'Are you sure? (y/n)'):
    print(text)
    inp = input(symbol + ' ')

    if inp in ['y','Y']:
        return True
    else:
        return False


class IPtable:
    def __init__(self):
        self.chains = self.__load_chains()
        self.rules = self.__load_rules()
        self.policies = self.__load_policies()
#        self.proto_list = self.__load_proto() 

    def __reload(self):
        self.chains = self.__load_chains()
        self.rules = self.__load_rules()
        self.policies = self.__load_policies()

    def __load_proto(self):
        unparsed_proto = subprocess.check_output("cat /etc/protocols", shell=True).decode()
        it = re.finditer(r'\n.*\d+.*', unparsed_proto)

        proto_list = ['any']

        for entry in it:
            entry = re.findall(r'.*\d+', unparsed_proto[entry.start():entry.end()])[0]
            entry = entry.split('\t')
            proto_list.append(entry[0].split(' ')[0])
            try:
                proto_list.append(entry[1])
            except:
                pass

        return list(set(proto_list))

    def __load_chains(self):
        unparsed_table = subprocess.check_output("sudo iptables -t filter -L", shell=True).decode()
        unparsed_table = unparsed_table.split('\n')

        chains = list()

        while unparsed_table:
            if re.search('Chain.*', unparsed_table[0]):
                chains.append(unparsed_table[0])

            del unparsed_table[0]

        return chains

    def __load_rules(self):
        rules = subprocess.check_output("sudo iptables -t filter -S", shell=True).decode()
        rules = re.findall('-A.*', rules)

        return rules

    def __load_policies(self):
        policy = subprocess.check_output("sudo iptables -t filter -S", shell=True).decode()
        policy = re.findall('-P.*', policy)

        return policy


    def subp_call_wrap(func):

        def wrapper(self, *args, **kwargs):
            subp_call_wrap = func(self, *args, **kwargs)
            if subp_call_wrap is True:
                print('Success')
            else:
                print('Error encountered')

        return wrapper

    def print_table(self):
        for chain in self.chains:
            print(chain)
            print(self.rule_header)
            if(self.table[chain][0] == ''):
                print('--EMPTY--')
            for rule in self.table[chain]:
                print(rule)

    def print_rules(self):
        for rule in self.rules:
            print(rule)

    def print_policies(self):
        for policy in self.policies:
            print(policy)

    def print_chains(self):
        for chain in self.chains:
            print(chain)

    def menu_screen(self):
        options = ['Add filter rule', 'Delete filter rule', 'Change policy',\
                'NAT rules', 'Backup config', 'Flush IP tables']

        while True:
            os.system('clear')
            print_section('IPTABLES MANAGER')
            print('0. Exit')
            print_enum_list(options)
            entry_idx = nr_input('\n->', 0, len(options))

            if entry_idx is 0:
                return
            elif entry_idx is 1:
                self.add_rule_menu()
            elif entry_idx is 2:
                self.change_menu('rule')
            elif entry_idx is 3:
                self.change_menu('policy')
            elif entry_idx is 4:
                self.nat_iface_menu()
            elif entry_idx is 5:
                self.__backup_conf('iptables_backup')
            elif entry_idx is 6:
                if yn_prompt(text='Are you sure? (y/n)') is True:
                    self.__flush_table()


    def sub_menu_screen(self, entry_list, header, cls = True):
        if cls: os.system('clear')
        
        print_section(header)
        print('0. <- Back')
        print_enum_list(entry_list)
        entry_idx = None

        while entry_idx is None:
            entry_idx = nr_input('\n->', 0, len(entry_list))
            if entry_idx is 0:
                return -1

        entry = entry_list[entry_idx-1]
        return entry
    
    @subp_call_wrap
    def add_rule(self, rule):
        try:
            subprocess.check_output('sudo iptables {}'.format(rule), shell=True)
            return True
        except subprocess.CalledProcessError as e:
            return False

    def add_nat_rules(self, rule_list):
        for rule in rule_list:
            print('\nAdding rule: ', rule)
            self.add_rule(rule)

    @subp_call_wrap
    def __backup_conf(self, path):
        try:
            subprocess.check_output('sudo iptables-save > {}'.format(path), shell=True)
            return True
        except subprocess.CalledProcessError as e:
            return False

    @subp_call_wrap
    def __save_persistent(self):
        try:
            subprocess.check_output('sudo service iptables save')
            return True
        except subprocess.CalledProcessError as e:
            return False

    @subp_call_wrap
    def change_policy(self, policy):
        policy = re.sub('^-P ', '--policy ', policy, 1)
        policy = re.sub('DROP|ACCEPT', '', policy, 1)
        inp = nr_input('\n1. ACCEPT\n2. DROP\n-> ', 1, 2)
        try:
            subprocess.check_output('sudo iptables {} {}'.format(policy, 'ACCEPT' if inp is 1 else 'DROP'), shell=True)
            return True
        except subprocess.CalledProcessError as e:
            return False

    @subp_call_wrap
    def delete_rule(self, rule):
        rule = re.sub('^-A ', '-D ', rule, 1)
        try:
            subprocess.check_output('sudo iptables {}'.format(rule), shell=True)
            return True
        except subprocess.CalledProcessError as e:
            return False

    def __rule_creator(self, chain):
        rule = ''
        source = def_input('Source address -> ', '0.0.0.0/0')
        dest = def_input('Destination address -> ', '0.0.0.0/0')
        proto = def_input('Protocol -> ', 'all')
        in_iface = None
        out_iface = None

        if chain == 'FORWARD':
            in_iface = def_input('Input interface -> ', 'any')
            out_iface = def_input('Output interface -> ', 'any')
        elif chain == 'INPUT':
            in_iface = def_input('Input interface -> ', 'any')
        elif chain == 'OUTPUT':
            out_iface = def_input('Output interface -> ', 'any')

        jump = def_input('Match rule -> ', 'ACCEPT')

        rule = '-A {} -s {} -d {} -j {} -p {}'.format(\
                chain, source, dest, jump, proto)

        if in_iface is not None:
            rule += ' -i ' + in_iface

        if out_iface is not None:
            rule += ' -o ' + out_iface
         
        return rule

    @subp_call_wrap
    def __flush_table(self):
        try:
            subprocess.check_output('sudo iptables --flush', shell=True)
            return True
        except subprocess.CalledProcessError as e:
            return False
    
    
    def __nat_rule_creator(self, rule_type, add_fl = True):
        nat_rules = []

        if rule_type is 'forward':
            source = def_input('Source address -> ', '0.0.0.0/0')
            in_iface = def_input('Input interface -> ', 'any')
            out_iface = def_input('Output interface -> ', 'any')
            proto = def_input('Protocol -> ', 'all')

            nat_rules.append('-{} FORWARD -s {} -i {} -o {} -p {} -m conntrack --ctstate NEW -j ACCEPT'.\
                    format('A' if add_fl is True else 'D', source, in_iface, out_iface, proto))
            nat_rules.append('-{} FORWARD -p {} -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT'.\
                    format('A' if add_fl is True else 'D', proto))

        elif rule_type is 'masquerade':
            iface = def_input('Interface -> ', 'eth0')
            nat_rules.append('-t nat -{} POSTROUTING -o {} -j MASQUERADE'.format('A' if add_fl is True else 'D', iface))

        return nat_rules


    def add_rule_menu(self):

        while True:
            chain = self.sub_menu_screen(self.chains, 'SELECT CHAIN')
            if chain is -1:
                return -1
            chain = re.findall('Chain (\w+)', chain)[0]

            rule = self.__rule_creator(chain)

            print('New rule: ', rule)

            rule_check = yn_prompt(text='\nAdd rule (y/n)?')
            
            if rule_check is True:
                self.add_rule(rule)
                continue_prompt()

    def nat_iface_menu(self, cls = True):
        if cls: os.system('clear')

        entry_list = ['Add NAT Forward rule', 'Delete NAT Forward rule', \
                'Add Masquerade to interface' , 'Delete Masquerade from interface']
        header = 'NAT Operations'
        entry = self.sub_menu_screen(entry_list, header)

        while entry != -1:
            if entry in entry_list[:2]:
                nat_rules = self.__nat_rule_creator('forward', \
                        True if entry is entry_list[0] else False)
                create_fl = yn_prompt(text='{} nat rule(y/n)?'.format(\
                        'Add' if entry is entry_list[0] else 'Delete'))
                if create_fl is True:
                    self.add_nat_rules(nat_rules)
                    continue_prompt();

            elif entry in entry_list[2:]:
                nat_rules = self.__nat_rule_creator('masquerade', \
                        True if entry is entry_list[2] else False)
                create_fl = yn_prompt(text='{} masquerade(y/n)?'.format(\
                        'Add' if entry is entry_list[2] else 'Delete'))
                if create_fl is True:
                    self.add_nat_rules(nat_rules)
                    continue_prompt();

            entry = self.sub_menu_screen(entry_list, header)

    def change_menu(self, entry_type):

        def get_entry_list():
            self.__reload()
            if entry_type is 'rule':
                return self.rules
            elif entry_type is 'policy':
                return self.policies
            else:
                return -1

        header = 'SELECT RULE' if entry_type is 'rule' else 'SELECT POLICY' 
        entry_list = get_entry_list()
        entry = self.sub_menu_screen(entry_list, header)

        while entry != -1:

            delete_fl = yn_prompt(text = '{}  {} (y/n)?'.format('Delete' if \
                    entry_type is 'rule' else 'Change', entry))
            
            if delete_fl is True and entry_type is 'rule':
                self.delete_rule(entry)
            if delete_fl is True and entry_type is 'policy':
                self.change_policy(entry)

            continue_prompt()

            entry_list = get_entry_list()
            entry = self.sub_menu_screen(entry_list, header)

def main():
    try:
        ipt = IPtable()
        ipt.menu_screen()
    except:
        print('Program exited unexceptedly')

main()
